# image-gallery-vue

This is simple image upload project created using imgur api. In this project, I include all the best practices to work with vuex, vue-router, lifecycle hooks and how to modularize async logic into small files so that our components and actions don't get messy. 😄

Here is a link to this project's website live https://eloquent-lewin-9b0fe2.netlify.com

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

This project is not working correctly because the imgur api has been changed. I'm not going to update it because there are no errors in the source code regarding vue. If you want to update it yourself and play with the code then you can read imgur api docs and go to upload section and try to find errors. 😀
